package de.hs.testclient;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.hs.chatlib.ChatLib;
import de.hs.chatlib.packet.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

@Slf4j
public class TestController extends Listener implements PrimaryStageAware {
  @FXML
  private ListView packetTypeList;
  @FXML
  private TextArea sendPacketText;
  @FXML
  private TextArea receivePacketText;

  private final Map<String, Packet> packetTypes = new HashMap<>();
  private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

  private String selectedPacketName;
  private ObservableList<String> observablePacketTypes;
  private Client client;
  private Stage primaryStage;

  private void addPacketClass(Packet packet) {
    packetTypes.put(packet.getClass().getSimpleName(), packet);
    observablePacketTypes.add(packet.getClass().getSimpleName());
  }

  @SneakyThrows
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    observablePacketTypes = FXCollections.observableList(new ArrayList<>(packetTypes.keySet()));

    addPacketClass(new LoginRequestPacket(1L, "secret"));
    addPacketClass(new ChatMessagePacket("", "", "This is a test message."));
    addPacketClass(new RegistrationRequestPacket("test", "test", "test@test.de"));

    packetTypeList.setItems(observablePacketTypes);

    client = new Client();
    ChatLib.init(client.getKryo());
    client.start();
    client.connect(5000, "127.0.0.1", 1337);
    client.addListener(this);
  }

  @Override
  public void received(Connection connection, Object object) {

    if (!(object instanceof Packet))
      return;

    if (object instanceof LoginResponsePacket) {
      var packet = (LoginResponsePacket) object;
      if (packet.isSuccess()) {
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            primaryStage.setTitle(primaryStage.getTitle() + " - " + packet.getNickname());
          }
        });
      }
    }

    receivePacketText.setText(gson.toJson(object));
  }

  public void onPacketTypeListClick(MouseEvent event) {
    if (event.getClickCount() == 2) {
      selectedPacketName = (String) packetTypeList.getSelectionModel()
              .getSelectedItem();

      Packet packet = packetTypes.get(selectedPacketName);
      sendPacketText.setText(gson.toJson(packet));
    }
  }

  public void onSendButtonClick(ActionEvent event) {
    Packet packet = gson.fromJson(sendPacketText.getText(), packetTypes.get(selectedPacketName).getClass());
    client.sendTCP(packet);
  }

  @Override
  public void setPrimaryStage(Stage primaryStage) {
    this.primaryStage = primaryStage;
  }
}
