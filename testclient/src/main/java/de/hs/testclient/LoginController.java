package de.hs.testclient;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import de.hs.chatlib.ChatLib;
import de.hs.chatlib.packet.LoginRequestPacket;
import de.hs.chatlib.packet.LoginResponsePacket;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.SneakyThrows;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController extends Listener implements Initializable {

  @FXML
  private TextField TextFieldName;
  @FXML
  private TextField TextFieldPassword;
  @FXML
  private Label LabelAuthFailed;

  private Client client;

  @SneakyThrows
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    client = new Client();
    ChatLib.init(client.getKryo());
    client.start();
    client.connect(5000, "127.0.0.1", 1337);
    client.addListener(this);
  }

  @SneakyThrows
  @Override
  public void received(Connection connection, Object object){
    if (object instanceof LoginResponsePacket) {
      LoginResponsePacket response = (LoginResponsePacket) object;
      if (response.isSuccess()){
        System.out.println("Login Success");
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            switchScene();
          }
        });
      }
      else {
        System.out.println("Login Fail");
        Platform.runLater(new Runnable() {
          @Override
          public void run() {

            LabelAuthFailed.setText("Tja Machste Nix, for shure");
          }
        });
      }
    }
  }

  public void onLoginButtonClick(ActionEvent event) {
//    client.sendTCP(new LoginRequestPacket(TextFieldName.getText(), TextFieldPassword.getText()));
  }

  @SneakyThrows
  public void switchScene() {
    Stage stage = (Stage) TextFieldName.getScene().getWindow();
    Parent root = FXMLLoader.load(getClass().getResource("test.fxml"));

    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.show();
  }
}

