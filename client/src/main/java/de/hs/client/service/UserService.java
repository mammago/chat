package de.hs.client.service;

import de.hs.client.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class UserService {
    private static UserService instance;
    private List<Contact> contacts;
    private Contact me;

    private UserService() {
        contacts = new ArrayList<>();
    }

    public static UserService getInstance() {
        if(instance == null)
            instance = new UserService();

        return instance;
    }

    public Contact getMe() {
        return me;
    }

    public void setMe(Contact me) {
        this.me = me;
    }

    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    public Contact getContactByUin(Long uin) {
        return contacts.stream().filter(c -> c.getUin() == uin).findFirst().orElse(null);
    }
}
