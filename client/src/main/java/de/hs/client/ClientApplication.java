package de.hs.client;

import de.hs.client.extension.SceneManager;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClientApplication extends Application {

    @Override
    public void start(Stage stage) {
        stage.setWidth(250);
        SceneManager sceneManager = new SceneManager(stage, createDummyStage());
        sceneManager.switchScene("login");
        stage.setTitle("7 QCI");
        stage.getIcons().add(new Image("/assets/images/icq_online.png"));
        stage.setOnCloseRequest((e) -> {
            ServerConnection.getInstance().disconnect();
            Platform.exit();
            System.exit(1);
        });
        stage.show();
    }

    public Stage createDummyStage() {
        Stage dummyPopup = new Stage();
        dummyPopup.initModality(Modality.NONE);
        // set as utility so no iconification occurs
        dummyPopup.initStyle(StageStyle.UTILITY);
        // set opacity so the window cannot be seen
        dummyPopup.setOpacity(0d);
        // not necessary, but this will move the dummy stage off the screen
        final Screen screen = Screen.getPrimary();
        final Rectangle2D bounds = screen.getVisualBounds();
        dummyPopup.setX(bounds.getMaxX());
        dummyPopup.setY(bounds.getMaxY());
        // create/add a transparent scene
        final Group root = new Group();
        dummyPopup.setScene(new Scene(root, 1d, 1d, Color.TRANSPARENT));
        // show the dummy stage
        dummyPopup.show();
        return dummyPopup;
    }

    public static void main(String[] args) {
        launch();
    }

}