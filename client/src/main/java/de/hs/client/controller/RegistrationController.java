package de.hs.client.controller;

import com.esotericsoftware.kryonet.Connection;
import de.hs.chatlib.packet.Packet;
import de.hs.chatlib.packet.RegistrationRequestPacket;
import de.hs.chatlib.packet.RegistrationResponsePacket;
import de.hs.client.model.Contact;
import de.hs.client.service.UserService;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Slf4j
public class RegistrationController extends ControllerBase {
    @FXML
    public TextField txtNickname;

    @FXML
    public PasswordField txtPassword1;

    @FXML
    public PasswordField txtPassword2;

    @FXML
    public TextField txtEmail;

    @FXML
    public Label lblErrorMessage;

    @FXML
    public ImageView imageViewAvatar;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void onRegisterButtonClicked() {
        if (txtNickname.getText().length() < 1
                || txtPassword1.getText().length() < 1
                || txtPassword2.getText().length() < 1
                || txtEmail.getText().length() < 1) {
            lblErrorMessage.setText("Please fill in all fields.");
            return;
        }

        if(txtNickname.getText().length() > 25) {
            lblErrorMessage.setText("Nickname must be 25 character or less.");
            return;
        }

        if(txtPassword1.getText().length() > 200) {
            lblErrorMessage.setText("Password to long.");
            return;
        }

        if(txtPassword2.getText().length() > 200) {
            lblErrorMessage.setText("Password to long.");
            return;
        }

        if(!txtEmail.getText().contains("@")
                || txtEmail.getText().length() > 200) {
            lblErrorMessage.setText("Please enter a valid email address.");
            return;
        }

        if (!txtPassword1.getText().equals(txtPassword2.getText())) {
            lblErrorMessage.setText("Passwords don't match.");
            return;
        }

        try {
            serverConnection.sendPacket(new RegistrationRequestPacket(
                    txtNickname.getText(),
                    txtPassword1.getText(),
                    txtEmail.getText(),
                    imageToByteArray(imageViewAvatar.getImage())));
        } catch (IOException e) {
            lblErrorMessage.setText("Couldn't convert avatar image.\nPlease try a different one.");
        }
    }

    @Override
    public void packetReceived(Connection connection, Packet packet) {
        if (packet instanceof RegistrationResponsePacket) {
            var registrationResponse = (RegistrationResponsePacket) packet;

            if (registrationResponse.isSuccess()) {
                UserService.getInstance().setMe(new Contact(registrationResponse.getUin(), null));
                Platform.runLater(this::showLoginScreen);
            } else {
                Platform.runLater(() -> lblErrorMessage.setText(registrationResponse.getMessage()));
            }
        }
    }

    private void showLoginScreen() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "User created successfully.", ButtonType.OK);
        alert.showAndWait();

        sceneManager.switchScene("login", true);
    }

    public void onBackButtonClicked() {
        sceneManager.switchScene("login");
    }

    public void onAvatarClick(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() != MouseButton.PRIMARY)
            return;

        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png")
        );

        File selectedFile = fileChooser.showOpenDialog(lblErrorMessage.getScene().getWindow());

        if(selectedFile == null)
            return;

        var image = scale(new Image(selectedFile.toURI().toString()), 128, 128, false);
        imageViewAvatar.setImage(image);
    }

    private Image scale(Image source, int targetWidth, int targetHeight, boolean preserveRatio) {
        ImageView imageView = new ImageView(source);
        imageView.setPreserveRatio(preserveRatio);
        imageView.setFitWidth(targetWidth);
        imageView.setFitHeight(targetHeight);
        return imageView.snapshot(null, null);
    }

    private byte[] imageToByteArray(Image image) throws IOException {
        BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
        ByteArrayOutputStream s = new ByteArrayOutputStream();
        ImageIO.write(bImage, "png", s);
        byte[] res  = s.toByteArray();
        s.close();
        return res;
    }
}
