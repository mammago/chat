package de.hs.client.controller;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import de.hs.chatlib.packet.Packet;
import de.hs.client.ServerConnection;
import de.hs.client.extension.SceneManager;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ControllerBase extends Listener implements Initializable {
    protected SceneManager sceneManager;
    protected ServerConnection serverConnection;
    protected Stage notificationStage;

    public ControllerBase() {
        serverConnection = ServerConnection.getInstance();
        serverConnection.addListener(this);
    }

    @Override
    public void received(Connection connection, Object object) {
        if(object instanceof Packet) {
            log.info("Received packet {}", object);
            packetReceived(connection, (Packet) object);
        }
    }

    public abstract void packetReceived(Connection connection, Packet packet);

    public void setSceneManager(SceneManager sceneManager) {
        this.sceneManager = sceneManager;
    }

    public void setNotificationStage(Stage notificationStage) {
        this.notificationStage = notificationStage;
    }
}
