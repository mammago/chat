package de.hs.client.controller;

import com.esotericsoftware.kryonet.Connection;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.hs.chatlib.packet.*;
import de.hs.client.extension.ContactCellFactory;
import de.hs.client.model.Contact;
import de.hs.client.service.UserService;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.action.Action;

import javax.sound.sampled.*;
import java.io.*;
import java.net.URL;
import java.util.*;

@Slf4j
public class HomeController extends ControllerBase {
  @FXML
  public ImageView imgMyAvatar;

  @FXML
  private ListView<Contact> friendlist;

  @FXML
  private Text lblMyNickname;

  private final Map<String, Stage> openChats = new HashMap<>();
  private final Gson gson = new GsonBuilder().create();

  private ObservableList<Contact> observableContacts;
  private Contact me;

  @SneakyThrows
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    me = UserService.getInstance().getMe();
    lblMyNickname.setText(me.getNickname());

    observableContacts = FXCollections.observableList(new ArrayList<>());
    friendlist.setCellFactory(new ContactCellFactory());
    friendlist.setItems(observableContacts);

    serverConnection.sendPacket(new FriendListRequestPacket());
    serverConnection.sendPacket(new AvatarRequest(me.getUin()));
    playSound("/assets/sounds/icq_login.wav");
  }

  @Override
  public void packetReceived(Connection connection, Packet packet) {
    if (packet instanceof FriendStatusPacket) {
      handleFriendStatusPacket((FriendStatusPacket) packet);
    }

    if(packet instanceof AvatarResponse) {
      handleAvatarResponse((AvatarResponse)packet);
    }

    if (packet instanceof ChatMessagePacket) {
      handleChatMessagePacket((ChatMessagePacket) packet);
    }
  }

  private void handleAvatarResponse(AvatarResponse packet) {
    String filePath = packet.getUin() + ".png";
    File file = new File(filePath);
    try {
      file.createNewFile();
      OutputStream outputStream = new FileOutputStream(file);
      outputStream.write(packet.getAvatar());
      outputStream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    if(packet.getUin().equals(me.getUin())) {
      imgMyAvatar.setImage(new Image(file.toURI().toString()));
    }
  }

  private void handleFriendStatusPacket(FriendStatusPacket packet) {
    serverConnection.sendPacket(new AvatarRequest(packet.getUin()));
    Platform.runLater(() ->
            findContactByUin(packet.getUin())
                    .ifPresentOrElse(
                            (contact -> updateContact(new Contact(packet.getUin(), packet.getNickname(), packet.getStatus()))),
                            () -> {
                              observableContacts.add(new Contact(packet.getUin(), packet.getNickname(), packet.getStatus()));
                              Collections.sort(observableContacts);
                            }
                    ));
  }

  @SneakyThrows
  private void handleChatMessagePacket(ChatMessagePacket packet) {
    if (!openChats.containsKey(packet.getFrom())) {
      String gsonPacket = gson.toJson(packet) + "\n";
      String messages = packet.getFrom() + ".json";
      File file = new File(messages);
      try {
        file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
      FileWriter fileWriter = new FileWriter(messages, true);
      fileWriter.write(gsonPacket);
      fileWriter.close();
      playSound("/assets/sounds/icq_message.wav");

      var contactOptional = findContactByNickname(packet.getFrom());
      if (contactOptional.isEmpty())
        return;

      var contact = contactOptional.get();
      contact.setHasPendingMessages(true);
      updateContact(contact);

      showMessageNotification(contact);
    }
  }

  private void showMessageNotification(Contact contact) {
    Platform.runLater(() -> {
      Notifications.create()
              .title("New message from " + contact.getNickname())
              .text("Click here to view the message.")
              .owner(notificationStage)
              .hideAfter(Duration.seconds(3))
              .graphic(new ImageView(new Image("/assets/images/icq_online.png")))
              .action(new Action("View", (e) -> openChatWindow(contact)))
              .show();
    });
  }

  @SneakyThrows
  private void openChatWindow(Contact contact) {
    contact.setHasPendingMessages(false);
    updateContact(contact);

    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/chat.fxml"));
    Stage stage = new Stage();
    Parent root = fxmlLoader.load();
    ChatController controller = fxmlLoader.getController();
    controller.setChatPartner(contact);
    controller.setMe(me);

    File file = checkForMessages(contact.getNickname());
    if (file != null) {
      try {
        controller.readMesssagesFromFile(file);
      } catch (IOException e) {
        e.printStackTrace();
      }
      controller.setMessagesFile(file);
    }

    stage.setOnCloseRequest(e -> openChats.remove(contact.getNickname()));
        stage.setOnCloseRequest(e -> {
          openChats.remove(contact.getNickname());
          controller.close();
        });

    stage.setTitle(contact.getNickname());
    stage.setScene(new Scene(root, 500, 400));
    stage.show();

    openChats.put(contact.getNickname(), stage);
  }

  public void onListClick(MouseEvent event) {
    if (event.getClickCount() == 2) {
      Contact contact = friendlist.getSelectionModel().getSelectedItem();
      friendlist.getSelectionModel().clearSelection();

      if(contact == null)
        return;

      if (!openChats.containsKey(contact.getNickname())) {
        openChatWindow(contact);
      } else {
        Stage stage = openChats.get(contact.getNickname());
        stage.setAlwaysOnTop(true);
        stage.setAlwaysOnTop(false);
        stage.show();
      }
    }

    if (event.getClickCount() == 1) {
      friendlist.getSelectionModel().clearSelection();
    }
  }

  private void updateContact(Contact contact) {
    var contactOptional = findContactByUin(contact.getUin());
    if (contactOptional.isEmpty())
      return;

    var oldContact = contactOptional.get();
    Platform.runLater(() -> {
      observableContacts.remove(oldContact);

      observableContacts.add(contact);
      Collections.sort(observableContacts);
      friendlist.refresh();
    });
  }

  private Optional<Contact> findContactByUin(Long uin) {
    return observableContacts.stream()
            .filter(contact -> contact.getUin().equals(uin))
            .findFirst();
  }

  private Optional<Contact> findContactByNickname(String nickname) {
    return observableContacts.stream()
            .filter(contact -> contact.getNickname().equals(nickname))
            .findFirst();
  }

  private void playSound(String url) {
    AudioInputStream audioIn;
    try {
      audioIn = AudioSystem.getAudioInputStream(getClass().getResource(url));
      Clip clip = AudioSystem.getClip();
      clip.open(audioIn);
      clip.start();
    } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
      e.printStackTrace();
    }
  }

  private File checkForMessages(String nickname) {
    File file = new File(nickname + ".json");
    boolean exists = file.exists();
    if (exists) return file;
    else return null;
  }
}