package de.hs.client;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Listener;
import de.hs.chatlib.ChatLib;
import de.hs.chatlib.packet.Packet;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class ServerConnection {
  private static ServerConnection instance;

  private final Client client;

  private ServerConnection() {
    client = new Client(65000, 65000);
    ChatLib.init(client.getKryo());
  }

  public void sendPacket(Packet packet) {
    client.sendTCP(packet);
    log.info("Sent packet {}", packet);
  }

  public void addListener(Listener listener) {
    client.addListener(listener);
  }

  public void removeListener(Listener listener) {
    client.removeListener(listener);
  }

  public void connect(String ip, int port) throws IOException {
    client.start();
    client.connect(5000, ip, port);
  }

  public void disconnect() {
    client.close();
  }

  public boolean isConnected() {
    return client.isConnected();
  }

  public static ServerConnection getInstance() {
    if(instance == null)
      instance = new ServerConnection();

    return instance;
  }
}
