package de.hs.client.model;

import de.hs.chatlib.packet.OnlineStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Contact implements Comparable<Contact> {
    private Long uin;
    private String nickname;
    private OnlineStatus onlineStatus;
    private boolean hasPendingMessages;

    public Contact(Long uin, String nickname) {
        this.uin = uin;
        this.nickname = nickname;
    }

    public Contact(Long uin, String nickname, OnlineStatus onlineStatus) {
        this.uin = uin;
        this.nickname = nickname;
        this.onlineStatus = onlineStatus;
    }

    @Override
    public int compareTo(Contact other) {
        return this.nickname.toLowerCase().compareTo(other.getNickname().toLowerCase());
    }
}
