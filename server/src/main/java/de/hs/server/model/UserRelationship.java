package de.hs.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRelationship {
  private Long relatingUserId;
  private Long relatedUserId;
  private Relationship relationship;
}
