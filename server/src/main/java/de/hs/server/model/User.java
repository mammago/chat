package de.hs.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
  private Long UIN;
  private String nickname;
  private String password;
  private String email;

  private List<UserRelationship> relationships;
}
