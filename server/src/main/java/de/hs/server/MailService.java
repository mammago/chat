package de.hs.server;

import de.hs.server.model.User;
import lombok.SneakyThrows;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Properties;

public class MailService {
    private static MailService instance;
    private final String KEY_AUTH = "mail.smtp.auth";
    private final String KEY_TLS = "mail.smtp.starttls.enable";
    private final String KEY_HOST = "mail.smtp.host";
    private final String KEY_PORT = "mail.smtp.port";
    private final String KEY_TRUST = "mail.smtp.ssl.trust";
    private final String KEY_USERNAME = "mail.username";
    private final String KEY_PASSWORD = "mail.password";
    private final Properties applicationProperties;
    private final Properties mailProperties;
    private final String username;
    private final String password;
    private MailService() {
        applicationProperties = ApplicationProperties.get();
        mailProperties = new Properties();
        mailProperties.put(KEY_AUTH, applicationProperties.get(KEY_AUTH));
        mailProperties.put(KEY_TLS, applicationProperties.get(KEY_TLS));
        mailProperties.put(KEY_HOST, applicationProperties.get(KEY_HOST));
        mailProperties.put(KEY_PORT, applicationProperties.get(KEY_PORT));
        mailProperties.put(KEY_TRUST, applicationProperties.get(KEY_TRUST));

        username = applicationProperties.getProperty(KEY_USERNAME);
        password = applicationProperties.getProperty(KEY_PASSWORD);
    }

    public static MailService getInstance() {
        if(instance == null)
            instance = new MailService();

        return instance;
    }

    public void sendConfirmationMail(User user) throws MessagingException {
        Session session = Session.getInstance(mailProperties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username));
        message.setRecipients(
                Message.RecipientType.TO, InternetAddress.parse(user.getEmail()));
        message.setSubject("7 CQI Account Confirmation");

        String msg = "Dear ${NICKNAME},<br/>your 7 QCI account was created.<br/>You can log in using your UIN <b>${UIN1} ${UIN2} ${UIN3}</b>.<br/>Have fun!";
        msg = msg.replace("${NICKNAME}", user.getNickname());

        String uin = Long.toString(user.getUIN());
        String uin1 = uin.substring(0, 3);
        String uin2 = uin.substring(3, 6);
        String uin3 = uin.substring(6, 9);
        msg = msg.replace("${UIN1}", uin1);
        msg = msg.replace("${UIN2}", uin2);
        msg = msg.replace("${UIN3}", uin3);

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(msg, "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        message.setContent(multipart);

        Transport.send(message);
    }
}
