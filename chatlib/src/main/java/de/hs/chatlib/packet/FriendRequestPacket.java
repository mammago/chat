package de.hs.chatlib.packet;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FriendRequestPacket extends Packet {
  private Long from;
  private Long to;
}
