package de.hs.chatlib.packet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AvatarResponse extends Packet {
    private Long uin;
    private byte[] avatar;
}
